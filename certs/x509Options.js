'use strict'
const winston = require('winston');
const fs = require('fs');

// path to CA cert and key pass
const caCertPath = "./certs/ca.itsfullofstars.de.crt.pem";
const caCertKeyPath = "./certs/ca.itsfullofstars.de.key.pem";
const caKeyPass = "test123";

/**
 * x509options module
 *
 * @module x509options
 */

// logging
const loggers = {
    splat: winston.createLogger({
      level: 'info',
      format: winston.format.combine(
        winston.format.splat(),
        winston.format.simple()
      ),
      transports: [new winston.transports.Console()],
    }),
    log: winston.createLogger({
      level: 'info',
      format: winston.format.simple(),
      transports: [new winston.transports.Console()],
    })
};

/**
 * Set X509 options object
 * @param {string} userId 
 * @param {string} email 
 * @returns {object} X509 configuration
 */
function getX509Options(userId, email) {

    // CA cert
    const caCrt = fs.readFileSync(caCertPath, "utf8");    
    const caKey = fs.readFileSync(caCertKeyPath, "utf8");    
  
    let x509Options = {
      country: "DE",
      state: "BW",
      userId: userId,
      email: email,
      caCrt: caCrt,
      caKey: caKey,
      caKeyPass: caKeyPass,
      days: 1
    };
  
    return x509Options;
  }
  

// export functions
module.exports.getX509Options = getX509Options;