'use strict'

const winston = require('winston'); // logging
const pem = require('pem'); // openssl

/**
 * x509 module
 *
 * @module x509
 */

// logging
const loggers = {
    splat: winston.createLogger({
      level: 'info',
      format: winston.format.combine(
        winston.format.splat(),
        winston.format.simple()
      ),
      transports: [new winston.transports.Console()],
    }),
    log: winston.createLogger({
      level: 'info',
      format: winston.format.simple(),
      transports: [new winston.transports.Console()],
    })
};


/**
 * Creates a certificate request
 *
 * @static
 * @param {Object} [options] Optional options object
 * @param {String} [options.country] Country
 * @param {String} [options.state] State
 * @param {String} [options.locality] Locality
 * @param {String} [options.organization] Organization
 * @param {String} [options.organizationUnit] org unit
 * @param {String} [options.commonName] CN
 * @param {String} [options.emailAddress] Email address
 */
function createCSR(options) {
    
    if (typeof options !== 'object') {        
        options = undefined;
    }
    options = options || {};
    loggers.splat.debug('options %j', options);

    return new Promise(function(resolve, reject) {
        var csrReq = pem.promisified.createCSR(
            {
                keyBitsize: 1024,
                country: options.country || "DE",
                state: options.state || "BW",
                locality: options.locality || "",
                organization: options.organization || "",
                organizationUnit: options.organizationUnit || "",
                commonName: options.userId || "",
                emailAddress: options.email || ""
            });

        csrReq.then((keys) => {
            //loggers.log.debug('CSR created');
            //loggers.splat.debug('%j', keys.csr);
            //loggers.log.debug('CSR key');
            //loggers.splat.debug('%j', keys.clientKey);

            resolve(keys);
        })
        .catch(err => reject(err));
    });
};

/**
 * Sign certificate request
 
 * @static
 * @param {Object} [options] Mandatory options object
 * @param {String} [options.caCrt] CA public cert
 * @param {String} [options.caKey] CA private key
 * @param {String} [options.caKeyPass] CA private key passphrase
 * @param {String} [options.serial] Serial number
 * @param {String} [options.csr] CSR request to sign
 * @param {String} [options.days] Days certificate will be valid
 */
function createX509(options) {
    if (typeof options !== 'object' || options === undefined) {        
        return;
    }
    loggers.splat.debug('options %j', options);

    return new Promise(function(resolve, reject) {
        var crtReq = pem.promisified.createCertificate(
            {
                serviceCertificate: options.caCrt,
                serviceKey: options.caKey,
                serviceKeyPassword: options.caKeyPass,
                serial: options.serial,
                csr: options.csr,
                days: options.days
            });
          
        crtReq.then((keys) => {
            //loggers.log.debug('Client key');
            //loggers.splat.debug('%j', keys.clientKey);
            //loggers.log.debug('Client certificate');
            //loggers.splat.debug('%j', keys.certificate);
            
            resolve(keys);
        })
        .catch(err => reject(err));
    });
};

/**
 * Get signed X.509 certificate
 * 
 * @param {object} options 
 */
async function getX509(options) {
    // get CSR
    const csr = await this.createCSR(options);
    // Set additional options for certificate signing
    options.csr = csr.csr;
    options.serial = Date.now();
    // get signed certificate
    let x509 = await createX509(options);
    x509.clientKey = csr.clientKey;

    return {key: x509.clientKey, cert: x509.certificate};
};

// export functions
module.exports.createCSR = createCSR;
module.exports.createX509 = createX509;
module.exports.getX509 = getX509;