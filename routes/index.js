var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'OpenID Connect Proxy for Logon to SAP backend via X.509' });
});

module.exports = router;
