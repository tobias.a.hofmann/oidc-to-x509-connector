const express = require('express');
const router = express.Router();
const x509 = require('../certs/x509');
const x509options = require('../certs/x509Options');
const oidcProxy = require('../proxy/proxy');

/**
 * Checks if user is logged on or not
 * Express: the existance of
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 * @returns 
 */
const loggedOn = function (req, res, next) {
  if (req.user) { 
    return next(); 
  } else {
    req.session.returnTo = req.originalUrl;
    res.redirect('/login');
  }
};

/* GET api proxy. */
router.get('/api*', loggedOn, function (req, res, next) {
  
  if (req.session.x509) {
    console.log("User session and X.509 exists.");
    oidcProxy.proxyRequestLogin(req, res, req.session.x509, "/api");
  } else {
    console.log("User session does not exist.");
    console.log("Create a new X.509 for the user and add it to current session.");
  
    // create X.509 cert to log on user
    const { _raw, _json, ...userProfile } = req.user;

    // debug infos
    //console.log(userProfile);
    //console.log(userProfile.emails[0].value);
    
    // user profile
    let userId = userProfile.nickname;
    let email = userProfile.emails[0].value;

    const x509Options = x509options.getX509Options(userId, email);

    // create X.509 certificate
    const cert = x509.getX509(x509Options);
    cert.then(x509 => {
      // debug infos
      //console.log("PRIVATE KEY");
      //console.log(x509.key);
      //console.log("CERTIFICATE");
      //console.log(x509.cert);
      
      // store X509 in user session
      req.session.x509 = {};
      req.session.x509.cert = x509.cert;
      req.session.x509.key = x509.key;

      oidcProxy.proxyRequestLogin(req, res, x509, "/api");
    });
  }
});

module.exports = router;
