const express = require('express');
const router = express.Router();
const jwt = require('express-jwt');
const jwksRsa = require('jwks-rsa');
const x509 = require('../certs/x509');
const x509options = require('../certs/x509Options');
const oidcProxy = require('../proxy/proxy');

require('dotenv').config();


const checkJwt = jwt({
  secret: jwksRsa.expressJwtSecret({
    cache: true,
    rateLimit: true,
    jwksRequestsPerMinute: 5,
    jwksUri: `https://${process.env.AUTH0_DOMAIN}/.well-known/jwks.json`
  }),

  // Validate the audience and the issuer.
  audience: process.env.AUTH0_AUDIENCE,
  issuer: `https://${process.env.AUTH0_DOMAIN}/`,
  algorithms: ['RS256']
});

/**
 * Proxy for endpoint /apicc
 * Every request to to /apicc/<service> is proxied to backend /<service>
 * In case the user certificate is part of the session, it is used
 * If its the first request, a new X.509 is created and added to the session
 * As this is the client credentials endpoint, a generic X.509 is created
 */
router.get('/apicc*', checkJwt, function(req, res) {
  console.log(req.user);

  if (req.session.x509) {
    console.log("session existiert");
    oidcProxy.proxyRequestLogin(req, res, req.session.x509, "/apicc");
  } else {

    // create X.509 for dummy user

    // user profile
    const userId = "tobias";
    const email = "tobias@itsfullofstars.de";
    const x509Options = x509options.getX509Options(userId, email);
    // create X.509 certificate
    const cert = x509.getX509(x509Options);

    cert.then(x509 => {
      // debug infos
      //console.log("PRIVATE KEY");
      //console.log(x509.key);
      //console.log("CERTIFICATE");
      //console.log(x509.cert);
      
      // store X509 in user session
      req.session.x509 = {};
      req.session.x509.cert = x509.cert;
      req.session.x509.key = x509.key;

      oidcProxy.proxyRequestLogin(req, res, x509, "/apicc");
    });
  }
});

module.exports = router;
