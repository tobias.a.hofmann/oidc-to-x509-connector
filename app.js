const express = require('express');
const path = require('path');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const dotenv = require('dotenv');
// security
const session = require('express-session');
const passport = require('passport');
const Auth0Strategy = require('passport-auth0');
const flash = require('connect-flash');
// routes
const authRouter = require('./routes/auth');
const indexRouter = require('./routes/index');
const apiRouter = require('./routes/api');
const apiccRouter = require('./routes/apicc');

// setup using .env
dotenv.config();

// Configure Passport to use Auth0
const strategy = new Auth0Strategy(
  {
    domain: process.env.AUTH0_DOMAIN,
    clientID: process.env.AUTH0_CLIENT_ID,
    clientSecret: process.env.AUTH0_CLIENT_SECRET,
    audience: process.env.AUTH0_AUDIENCE,
    callbackURL:
      process.env.AUTH0_CALLBACK_URL || 'https://server.itsfullofstars.de:8443/callback'
  },
  function (accessToken, refreshToken, extraParams, profile, done) {    
    return done(null, profile);
  }
);

passport.use(strategy);

// You can use this section to keep a smaller payload
passport.serializeUser(function (user, done) {
  done(null, user);
});

passport.deserializeUser(function (user, done) {
  done(null, user);
});

const app = express();

// View engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(cookieParser());

// config express-session
const sess = {
  secret: 'CHANGE THIS SECRET',
  cookie: {},
  resave: false,
  saveUninitialized: true
};

if (app.get('env') === 'production') {
  sess.cookie.secure = true; // serve secure cookies, requires https
}

app.use(session(sess));
app.use(passport.initialize());
app.use(passport.session());
//app.use(express.static(path.join(__dirname, 'public')));
app.use(flash());
app.use('/', authRouter);
app.use('/', indexRouter);
app.use('/', apiccRouter);
app.use('/', apiRouter);

// info endpoint
app.get('/info', (req, res, next) => {
  res.send("Proxy service with OIDC to X.509 for SAP NW ABAP");
});

app._router.stack.forEach(function(r){
  if (r.route && r.route.path){
    console.log(r.route.path)
  }
})

module.exports = app;
