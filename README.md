# OIDC to X509 connector

An OpenID Connect to X.509 proxy. Takes a JWT token from a request and creates a X.509 certificate for it that is send to a backend to log on a user. Allows SSO for any backend that does not understand OIDC tokens, but X.509 based logons.

This connector is taking a OIDC JWT token from a client request and maps it to a X.509 certificate. In case of a user JWT, the user profile information user ID and e-mail are used to create the X.509. In case of a client credential JWT, a generic X.509 is created. After the X.509 is created, the certificate is added to the session of the user and the request is forwarded to the backend system, using the X.509 for authentication.

The connector was created for workshops to demonstrate how a SAP NetWeaver ABAP system can be accessed from OIDC clients that have a JWT token. NW ABAP isn't able to read JWT, therefore you normally need to use SAML 2.0 or find a workaround solution. The purpose of this connector is to enable scenarios with OIDC clients connecting to NW ABAP.



This connector was started based on the Auth0 example "regular web app", see [Quickstarts](https://auth0.com/docs/quickstart/webapp/nodejs/01-login). Currently the app uses not much more of the original code. In case you need to understand better how OIDC and JWT logons with a Node.js app work, please check the Auth0 documentation and referenced quickstart.

# Run

```sh
npm install
npm start
```

App will start on port 8443 and can be accessed on https://localhost:8443. You'll have to take of the TLS certificate the proxy connector uses. You might want to add server.itsfullofstars.de to your hosts file.

For NW ABAP you need a sample service that is enabled for X.509 logon. You might use the GWSAMPLE_BASIC service as a start. You need to enable this service for X.50 logons first!

```sh
/sap/opu/odata/IWBEP/GWSAMPLE_BASIC/?$format=xml
```

```sh
https://localhost:8443/api/sap/opu/odata/IWBEP/GWSAMPLE_BASIC/?$format=xml
https://localhost:8443/apicc/sap/opu/odata/IWBEP/GWSAMPLE_BASIC/?$format=xml
```

```sh
https://server.itsfullofstars.de:8443/api/sap/opu/odata/IWBEP/GWSAMPLE_BASIC/?$format=xml
https://server.itsfullofstars.de:8443/apicc/sap/opu/odata/IWBEP/GWSAMPLE_BASIC/?$format=xml
```

# Configuration

## General

The connector is based on an Auth0 tutorial. Therefore, the base configuration regarding OIDC assumes that you do have an Auth0 account and app/api configuration.

## Certificates

In the folder certs you find several PEM files. These are needed to start the connector with HTTPS as well as to issue a new X.509 based on the received JWT token and to connecto to the NW ABAP backend.

- server.itsfullofstars.de<key|crt>.de Server certificate and key for the connector. After starting the connector your web client (browser) can connect via HTTPS to it. This is the server certificate used by the connector.
- nwabap.dummy.nodomain.cer The server certificate from the backend NW ABAP system. You need to add here your backend server's certificate.
- ca.itsfullofstars.de<key|crt>.pem CA file. Both the connector server certificates and the issued client certificates are from this root CA. Your backend must trust this CA certificate to trust the X.509 client certificate send by the connector for logon.

## .env file

The .env is used to provide the configuration for the Auth0 app. I provide a sample file .env.sample that shows which parameters the app accepts. The values are obtained from your specific Auth0 app and API configuration. Without these values the app cannot validate your JWT token. If you do not know how to configure Auth0, I recommend to go through the quickstart tutorial listed above.

# Endpoints

The app exposes two endpoints for X.509 logon:

- /api/...
- /apicc/...

The api endpoints expects a JWT with user informations. From the JWT, the user ID and e-mail are extracted and used as inputs for the user certificate. This creates a SSO connection with the user information to the backend.

THE apicc endpoint expects a valid access token. A generic X.509 is created. That is, every user is logged on to the backend using the same user ID. This is configured in the file routes/apicc.js.

# Backend

This app is a connector procy to map incoming requests with JWT to a given backend that accepts X.509 certificates for logon. The proxy and backend is defined in the file proxy/proxy.js.

```javascript
const optionsProxy = { 
    target: {
      host: 'vhcalnplci.itsfullofstars.de',
      port: 44300,
      protocol: 'https:',
      key: x509.key,
      cert: x509.cert,
      ca: fs.readFileSync(caCertPath)      
    },
    secure: false,
    changeOrigin: true
  };
```

The hard coded backend is for the SAP NetWeaver 7.5x Developer Edition using Sybase DB. You'll have to adjust these parameters to map your environment.

// todo: this is going to be made configurable via .env file

# X.509

The folder certs contains a module for creating X.509 certificate: certs/x509.js. The function createCSR creates a CSR file (key + csr), createX509 issues a crt file for the csr - signed by the CA cert. My module is using the module [pem](https://www.npmjs.com/package/pem) to issue certificates. Be aware that pem is using openssl underneath. You do have to install openssl to make it work. You do not need to know how OpenSSL works, but you have to install it.

To make it as easy / transparent for you as possible, my module and the connector create everything from userID to CSR to a final signed CRT file for your. You do not need to worry about X.509 parameters, subjects, CN, Root CA, etc. The connector does this for you. To get a better understanding how it works I'll detail some aspects of the magic here.

The CSR file is configured to use the following values:

```json
{
    keyBitsize: 1024,
    country: options.country || "DE",
    state: options.state || "BW",
    locality: options.locality || "",
    organization: options.organization || "",
    organizationUnit: options.organizationUnit || "",
    commonName: options.userId || "",
    emailAddress: options.email || ""
}
```

**// todo:** make this configurable via .env file

The keysize is not very high, once for speed, and it is just a POC. The subject is fixed to be from Baden-Württemberg, while the other values are provided by the caller. Note: this is were the user ID and e-mail address are important!

The CA issuing is done in createX509.

```json
{
    serviceCertificate: options.caCrt,
    serviceKey: options.caKey,
    serviceKeyPassword: options.caKeyPass,
    serial: options.serial,
    csr: options.csr,
    days: options.days
}
```

The options provide the values for signing the CSR. Here the CA certificate is used, as well as the (dummy) password. The password for using the CA key is hardcoded in the file certs/x509Options.js

**// todo:** make this configurable via .env file

# Backend logon

For both API endpoints, the logon flow is the same. 

1. Check if a valid JWT token is provided
2. Create a X.509 for the user
3. Add this X.509 to the client session
4. Add the X.509 to the request to the backend (proxying)

Step 3 is the same in the files routes/api.js or routes/apicc.js. If a new X.509 is created - that is: it is the first request of the user to the backend. The X.509 certificate is added to the session of the user: req.session.

```javascript
req.session.x509 = {};
req.session.x509.cert = x509.cert;
req.session.x509.key = x509.key;
```

This value is than used by the proxy to forward the user cert in the HTTPS request to the backend.

```javascript
oidcProxy.proxyRequestLogin(req, res, x509, "/api");
```

In case the user already has a valid X.509 certificate, the request is forwarded directly to the backend.

```javascript
f (req.session.x509) {
    oidcProxy.proxyRequestLogin(req, res, req.session.x509, "/api");
} 
```

**Note:** this is only checking if the x509 object is part of the session object. It does _not_ validate if the certificate is still valid. It also does not check if the session in the backend is still valid. Short: if the backend session is invalidated, and the certificate not valid anylonger, the user won't be logged on!

# Example

(more detailed in a blog on my site) I have a sample CDS service that is enabled for X.509 logons in SICF.

https://server.itsfullofstars.de:8443/api/sap/opu/odata/sap/ZUSERINFO_SRV/?$format=xml

Calling this service throught the proxy connector:

https://server.itsfullofstars.de:8443/api/sap/opu/odata/sap/ZUSERINFO_SRV/?$format=xml

The mapping of the X.509 certificate to a valid SAP user is done in transaction CERTRULE. I map the CN name to an alias for user Developer.