'use strict'
const winston = require('winston');
const httpProxy = require('http-proxy');
const fs = require('fs');
// trust NW ABAP backend system
const caCertPath = "./certs/nwabap.dummy.nodomain.cer";

/**
 * proxy module
 *
 * @module proxy
 */

// logging
const loggers = {
  splat: winston.createLogger({
    level: 'info',
    format: winston.format.combine(
      winston.format.splat(),
      winston.format.simple()
    ),
    transports: [new winston.transports.Console()],
  }),
  log: winston.createLogger({
    levels: winston.config.syslog.levels,
    format: winston.format.simple(),
    transports: [new winston.transports.Console()]
  })
};


/**
 * 
 * @param {*} req 
 * @param {*} res 
 * @param {*} x509 
 */
function proxyRequestLogin(req, res, x509, proxyPath) {  
  loggers.log.debug("proxyRequestLogin");
  
  // proxy options
  const optionsProxy = { 
    target: {
      host: 'vhcalnplci.itsfullofstars.de',
      port: 44300,
      protocol: 'https:',
      key: x509.key,
      cert: x509.cert,
      ca: fs.readFileSync(caCertPath)      
    },
    secure: false,
    changeOrigin: true
  };

  // create proxy
  let proxy = httpProxy.createServer();

  // proxy events

  // proxy request
  proxy.on('proxyReq', function (proxyReq, req, res, options) {
    loggers.log.debug("proxyReq");

    const targetPath = req.url.replace(proxyPath, "");
    loggers.log.debug("target Path: " + targetPath);
    proxyReq.path = targetPath;
  });

  // proxy response
  proxy.on('proxyRes', function (proxyRes, req, res) {
    loggers.log.debug("proxyRes");
    loggers.log.debug("return status: " + res.statusCode);

    if (res.statusCode >= 200 && res.statusCode < 300) {}
  });

  // proxy error
  proxy.web(req, res, optionsProxy, function(err) {
    loggers.log.debug("proxy error");
    loggers.splat.debug("err %j", err);
  });
};

module.exports.proxyRequestLogin = proxyRequestLogin;
